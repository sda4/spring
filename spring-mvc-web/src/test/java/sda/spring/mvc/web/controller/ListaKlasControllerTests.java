package sda.spring.mvc.web.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import sda.spring.mvc.web.domain.Klasa;
import sda.spring.mvc.web.repository.KlasaRepository;
import sda.spring.mvc.web.repository.UczenRepository;

import java.util.Collections;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.contains;
import static org.mockito.BDDMockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@RunWith(SpringRunner.class)
@WebMvcTest(ListaKlasController.class)
@WithMockUser
public class ListaKlasControllerTests {

    @MockBean
    private KlasaRepository klasaRepository;

    @MockBean
    private UczenRepository uczenRepository;

    @Autowired
    private MockMvc mvc;

    @Test
    public void testujPustaListaKlas() throws Exception {
        given(klasaRepository.findAll()).willReturn(Collections.emptyList());

        mvc.perform(get("/klasy"))
                .andExpect(status().isOk())
                .andExpect(view().name("listaKlas"))
                .andExpect(model().attribute("listaKlas",
                        emptyCollectionOf(Klasa.class)));

        then(klasaRepository).should(only()).findAll();
        Mockito.verifyZeroInteractions(uczenRepository);
    }

    @Test
    public void testujZJednaKlasa() throws Exception {
        Klasa klasa = Klasa.builder()
                .idKlasy(1L)
                .symbol("A")
                .rokRozpoczeciaNauki(1999)
                .build();

        given(uczenRepository.countByKlasa(klasa)).willReturn(5L);
        given(klasaRepository.findAll())
                .willReturn(Collections.singletonList(klasa));

        mvc.perform(get("/klasy"))
                .andExpect(status().isOk())
                .andExpect(view().name("listaKlas"))
                .andExpect(model().attribute("listaKlas",
                        allOf(
                        hasSize(1),
                        contains(hasProperty("iloscUczniow", equalTo(5L))))));

        then(klasaRepository).should(only()).findAll();
        then(uczenRepository).should(only()).countByKlasa(klasa);
    }

    @Test
    public void testUsuwaNieistniejacaKlase() throws Exception {
        given(klasaRepository.findOne(5L)).willReturn(null);

        mvc.perform(get("/klasy/usun/5"))
                .andExpect(status().isOk())
                .andExpect(view().name("klasaNieIstnieje"));

        then(klasaRepository).should(only()).findOne(5L);
        verifyZeroInteractions(uczenRepository);
    }

    @Test
    public void testUsuwaNiepustaKlase() throws Exception {
        Klasa klasa = Klasa.builder().build();
        given(klasaRepository.findOne(5L)).willReturn(klasa);
        given(uczenRepository.countByKlasa(klasa)).willReturn(10L);

        mvc.perform(get("/klasy/usun/5"))
                .andExpect(status().isOk())
                .andExpect(view().name("niepustaKlasa"));

        then(klasaRepository).should(only()).findOne(5L);
        then(uczenRepository).should(only()).countByKlasa(klasa);
    }

    @Test
    public void testUsuwaPustaKlase() throws Exception {
        Klasa klasa = Klasa.builder().build();
        given(klasaRepository.findOne(5L)).willReturn(klasa);
        given(uczenRepository.countByKlasa(klasa)).willReturn(0L);

        mvc.perform(get("/klasy/usun/5"))
                .andExpect(status().isFound())
                .andExpect(redirectedUrl("/klasy"));

        then(klasaRepository).should(times(1)).findOne(5L);
        then(klasaRepository).should(times(1)).delete(klasa);
        verifyNoMoreInteractions(klasaRepository);

        then(uczenRepository).should(only()).countByKlasa(klasa);
    }
}
