package sda.spring.mvc.web.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import sda.spring.mvc.web.domain.Klasa;

@Repository
public interface KlasaRepository extends PagingAndSortingRepository<Klasa, Long> {

}
