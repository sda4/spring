package sda.spring.mvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import sda.spring.mvc.web.domain.Klasa;
import sda.spring.mvc.web.domain.Uczen;
import sda.spring.mvc.web.repository.KlasaRepository;
import sda.spring.mvc.web.repository.UczenRepository;

@Controller
public class ZapiszUczniaController {

    @Autowired
    private UczenRepository uczenRepository;

    @Autowired
    private KlasaRepository klasaRepository;

    @ModelAttribute("listaKlas")
    public Iterable<Klasa> wszystkieKlasy() {
        return klasaRepository.findAll();
    }


    @PostMapping("/uczniowie/zapiszNowego")
    public String zapiszNowegoUcznia(@ModelAttribute("nowy") @Validated  Uczen uczen,
                                     BindingResult br) {
        if (br.hasErrors()) {
            return "nowyUczen";
        } else {
            if (uczen.getIdKlasy() != null) {
                Klasa klasa = klasaRepository.findOne(uczen.getIdKlasy());
                uczen.setKlasa(klasa);
            }
            uczenRepository.save(uczen);

            return "redirect:/uczniowie";
        }
    }

    @PostMapping("/uczniowie/zapiszEdytowanego/{idUcznia}")
    public String zapiszNowegoUcznia(@PathVariable("idUcznia") Long idUcznia,
                                     @Validated  Uczen uczen,
                                     BindingResult br, Model model) {
        if (br.hasErrors()) {
            model.addAttribute("edytowany", uczen);
            return "edycjaUcznia";
        } else {
            if (uczen.getIdKlasy() != null) {
                uczen.setKlasa(klasaRepository.findOne(uczen.getIdKlasy()));
            }
            uczen.setIdUcznia(idUcznia);
            uczenRepository.save(uczen);

            return "redirect:/uczniowie";
        }
    }
}
