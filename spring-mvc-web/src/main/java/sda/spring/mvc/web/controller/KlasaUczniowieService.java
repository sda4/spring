package sda.spring.mvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.context.WebApplicationContext;
import sda.spring.mvc.web.domain.Klasa;
import sda.spring.mvc.web.domain.Uczen;
import sda.spring.mvc.web.repository.KlasaRepository;
import sda.spring.mvc.web.repository.UczenRepository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Component
@Scope(value = WebApplicationContext.SCOPE_SESSION,
        proxyMode = ScopedProxyMode.TARGET_CLASS)
public class KlasaUczniowieService {

    private Klasa klasa;

    private Uczen uczen;

    private List<Uczen> uczniowieKlasy;

    @Autowired
    private KlasaRepository klasaRepository;

    @Autowired
    private UczenRepository uczenRepository;

    @PostConstruct
    public void initSesja() {
        nowaKlasa();
    }

    public void nowaKlasa() {
        klasa = new Klasa();
        uczen = new Uczen();
        uczniowieKlasy = new ArrayList<>();
    }

    public void ustawKlase(Klasa klasa) {
        this.klasa = klasa;
    }

    @Transactional
    public void zapiszKlaseZUczniami() {
        Klasa zapisanaKlasa = klasaRepository.save(this.klasa);

        for (Uczen uczen : uczniowieKlasy) {
            uczen.setKlasa(zapisanaKlasa);

            uczenRepository.save(uczen);
        }
    }

    public void usunUcznia(int nrUcznia) {
        uczniowieKlasy.remove(nrUcznia);
    }

    public void dodajUcznia(Uczen uczen) {
        uczniowieKlasy.add(uczen);
    }

    public Klasa getKlasa() {
        return klasa;
    }

    public Uczen getUczen() {
        return uczen;
    }

    public List<Uczen> getUczniowieKlasy() {
        return uczniowieKlasy;
    }
}
