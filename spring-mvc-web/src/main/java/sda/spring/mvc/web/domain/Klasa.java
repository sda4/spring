package sda.spring.mvc.web.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Klasa {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idKlasy;

    @NotNull
    private Integer rokRozpoczeciaNauki;

    @NotEmpty
    private String symbol;

    @Transient
    private Long iloscUczniow;
}
