package sda.spring.mvc.web;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@Slf4j
@SpringBootApplication
public class MainWeb {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder()
                .sources(MainWeb.class)
                .run(args);
    }

    // zadania:
    // 1. utworzyć stronę i kontroler do wyświetlania listy uczniów
    // 1a. dodać tłumaczenia polskie, angielskie,... do powyższej strony
    // 2. utworzyć stronę i kontroler do dodawania nowego ucznia
    // 2a. dodać walidację pól imię, nazwisko, pesel - pola te nie mogą być puste
    // 3. umożliwić kasowanie ucznia z poziomu listy uczniów
    // 4. dodać możliwość edycji wybranego ucznia
    // 5. utworzyć stronę do dodawania nowej klasy
    // 5. dodać możliwość wyboru klasy dla ucznia na ekranie edycji ucznia
    // 6. dodać możliwość tworzenia nowej klasy od razu wraz z listą nowych uczniów do zapisania
    // 7. do powyższego punktu dodać możliwość przypisania istniejących uczniów (bez przypisanej klasy) do tworzonej klasy

}
