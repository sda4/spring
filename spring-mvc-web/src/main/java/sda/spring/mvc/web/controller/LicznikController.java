package sda.spring.mvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/licznik")
public class LicznikController {

    @Autowired
    private LicznikService licznikService;

    @GetMapping
    public String wyswietlLicznik(Model model) {
        model.addAttribute("zbiorTekstow",
                licznikService.getZbiorTekstow());
        return "licznik";
    }

    @PostMapping
    public String zwiekszLicznik(String wpisanyTekst, Model model) {
        licznikService.dodajTekst(wpisanyTekst);
        return "redirect:/licznik";
    }

    @GetMapping("/usun")
    public String usunTekst(@RequestParam("indeks") Integer indeks) {
        licznikService.usunTekst(indeks);
        return "redirect:/licznik";
    }

}
