package sda.spring.mvc.web.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import sda.spring.mvc.web.domain.Klasa;
import sda.spring.mvc.web.domain.Uczen;

@Repository
public interface UczenRepository extends PagingAndSortingRepository<Uczen, Long> {
    long countByKlasaIdKlasy(Long idKlasy);
    long countByKlasa(Klasa klasa);
}
