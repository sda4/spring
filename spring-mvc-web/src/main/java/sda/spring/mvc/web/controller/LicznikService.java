package sda.spring.mvc.web.controller;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

@Service
@Scope(value = WebApplicationContext.SCOPE_SESSION,
        proxyMode = ScopedProxyMode.TARGET_CLASS)
public class LicznikService {

    private List<String> zbiorTekstow = new ArrayList<>();

    public List<String> getZbiorTekstow() {
        return zbiorTekstow;
    }

    public void dodajTekst(String tekst) {
        zbiorTekstow.add(tekst);
    }

    public void usunTekst(int indeks) {
        zbiorTekstow.remove(indeks);
    }
}
