package sda.spring.mvc.web.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Uczen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUcznia;

    @NotEmpty
    private String imie;

    @NotEmpty
    private String nazwisko;

    @NotEmpty
    private String pesel;

    private Date dataUrodzenia;

    @ManyToOne
    private Klasa klasa;

    @Transient
    private Long idKlasy;
}
