package sda.spring.mvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import sda.spring.mvc.web.domain.Klasa;
import sda.spring.mvc.web.repository.KlasaRepository;
import sda.spring.mvc.web.repository.UczenRepository;

import java.security.Principal;

@Controller
@RequestMapping("/klasy")
public class ListaKlasController {

    @Autowired
    private KlasaRepository klasaRepository;

    @Autowired
    private UczenRepository uczenRepository;

    @GetMapping
    public String getListaKlas(Model model, Principal principal) {
        Iterable<Klasa> klasy = klasaRepository.findAll();
        for(Klasa klasa : klasy) {
            long iloscUczniow = uczenRepository.countByKlasa(klasa);
            klasa.setIloscUczniow(iloscUczniow);
        }
        model.addAttribute("listaKlas", klasy);
        return "listaKlas";
    }

    @GetMapping("/usun/{idKlasy}")
    public String usunKlase(@PathVariable("idKlasy") Long idKlasy) {
        Klasa klasa = klasaRepository.findOne(idKlasy);

        if(klasa != null) {
            Long iloscUczniow = uczenRepository.countByKlasa(klasa);

            if (iloscUczniow == 0) {
                klasaRepository.delete(klasa);

                return "redirect:/klasy";
            } else {
                return "niepustaKlasa";
            }
        } else {
            return "klasaNieIstnieje";
        }
    }
}
