package sda.spring.mvc.web.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import javax.sql.DataSource;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()

                .dataSource(dataSource);


//        auth.inMemoryAuthentication()
//                .withUser("user1").password("1234").roles("nauczyciel")
//                .and().withUser("user2").password("1234").roles("manager", "nauczyciel");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .formLogin()
                    .loginPage("/login-form")
                    .defaultSuccessUrl("/uczniowie")
                    .loginProcessingUrl("/login")
                    .usernameParameter("username")
                    .passwordParameter("password")
                    .failureUrl("/login-failure")
                .and().logout()
                    .logoutUrl("/wyloguj")
                    .logoutSuccessUrl("/uczniowie")
                .and()
                //.csrf().disable()
                .exceptionHandling().accessDeniedPage("/nie-wejdziesz")
                .and().authorizeRequests()
                    .antMatchers("/uczniowie").permitAll()
                    .antMatchers("/klasy").authenticated()
                    .antMatchers("/klasy/**/*").hasRole("manager");
    }
}
