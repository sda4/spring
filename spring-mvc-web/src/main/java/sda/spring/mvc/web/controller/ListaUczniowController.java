package sda.spring.mvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import sda.spring.mvc.web.domain.Uczen;
import sda.spring.mvc.web.repository.KlasaRepository;
import sda.spring.mvc.web.repository.UczenRepository;

import java.security.Principal;

@Controller
@RequestMapping("/uczniowie")
public class ListaUczniowController {

    @Autowired
    private UczenRepository uczenRepository;

    @Autowired
    private KlasaRepository klasaRepository;

    @GetMapping
    public String listaUczniow(Model model, Pageable pageable, Principal principal) {
        model.addAttribute("listaUczniow",
                uczenRepository.findAll(pageable));

        return "listaUczniow";
    }

    @GetMapping("/dodaj")
    public String dodajNowegoUcznia(Model model) {
        Uczen uczen = new Uczen();

        model.addAttribute("nowy", uczen);
        model.addAttribute("listaKlas", klasaRepository.findAll());

        return "nowyUczen";
    }

    @GetMapping("/edytuj/{idUcznia}")
    public String edytujUcznia(@PathVariable("idUcznia") Long idUcznia,
                               Model model) {
        Uczen uczen = uczenRepository.findOne(idUcznia);
        if (uczen.getKlasa() != null) {
            uczen.setIdKlasy(uczen.getKlasa().getIdKlasy());
        }

        model.addAttribute("edytowany",uczen);
        model.addAttribute("listaKlas", klasaRepository.findAll());

        return "edycjaUcznia";
    }

    @GetMapping("/usun/{idUcznia}")
    public String usunUcznia(@PathVariable("idUcznia") Long idUcznia) {
        Uczen uczen = uczenRepository.findOne(idUcznia);
        if (uczen != null) {
            uczenRepository.delete(idUcznia);

            return "redirect:/uczniowie";
        } else {
            return "uczenNieIstnieje";
        }
    }
}
