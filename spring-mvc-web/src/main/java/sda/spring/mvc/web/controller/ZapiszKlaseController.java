package sda.spring.mvc.web.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import sda.spring.mvc.web.domain.Klasa;
import sda.spring.mvc.web.domain.Uczen;
import sda.spring.mvc.web.repository.KlasaRepository;

import java.util.List;

@Controller
public class ZapiszKlaseController {

    @Autowired
    private KlasaRepository klasaRepository;

    @Autowired
    private KlasaUczniowieService klasaUczniowieService;

    @ModelAttribute("nowa")
    public Klasa nowaKlasa() {
        return klasaUczniowieService.getKlasa();
    }

    @ModelAttribute("nowy")
    public Uczen nowyUczen() {
        return klasaUczniowieService.getUczen();
    }

    @ModelAttribute("listaUczniow")
    public List<Uczen> listaUczniow() {
        return klasaUczniowieService.getUczniowieKlasy();
    }

    @GetMapping("/klasy/dodaj")
    public String dodajNowaKlase(Model model) {
        klasaUczniowieService.nowaKlasa();

        return "redirect:/klasy/klasaUczniowie";
    }

    @GetMapping("/klasy/klasaUczniowie")
    public String klasaUczniowie() {
        return "nowaKlasa";
    }

    @GetMapping("klasy/klasaUczniowie/usunUcznia")
    public String usunUcznia(@RequestParam("nrUcznia") Integer nrUcznia) {
        klasaUczniowieService.usunUcznia(nrUcznia);

        return "redirect:/klasy/klasaUczniowie";
    }

    @PostMapping("/klasy/klasaUczniowie/dodajNowego")
    public String dodajUczniaDoKlasy(@Validated Uczen uczen) {
        klasaUczniowieService.dodajUcznia(uczen);

        return "redirect:/klasy/klasaUczniowie";
    }

    @PostMapping("/klasy/zapiszNowa")
    public String zapiszNowaKlase(@ModelAttribute("nowa") @Validated Klasa klasa,
                                     BindingResult br) {

            klasaUczniowieService.ustawKlase(klasa);
            klasaUczniowieService.zapiszKlaseZUczniami();

            return "redirect:/klasy";
    }
}
