package sda.spring.core;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;

public class Main4 {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(Main4.class);

        // zadania:
        // 1. pozbyć się błędu NoUniqueBeanDefinitionException używając Qualifier
        // 2. pozbyć się błędu NoUniqueBeanDefinitionException wywalając autowiring
        // 3. dodać drugi bean typu Controller - czy metoda serviceA wywoła się drugi raz ?
        // 4. ponieważ bean serviceB nie jest wykorzystywany, sprawić by był tworzony w trybie lazy
    }

    @Bean
    public Controller controller() {
        return new Controller();
    }

    @Bean
    public Service serviceA() {
        return new Service();
    }

    @Bean
    @Lazy
    public Service serviceB() {
        return new Service();
    }

    public static class Controller {
        @Autowired
        private Service service;

        public void setService(Service service) {
            this.service = service;
        }
    }

    public static class Service {

    }
}
