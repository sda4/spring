package sda.spring.core.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "uczen")
public class Uczen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUcznia;

    private String imie;
    private String nazwisko;
    private String pesel;
}
