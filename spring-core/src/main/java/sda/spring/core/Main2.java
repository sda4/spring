package sda.spring.core;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import sda.spring.core.domain.Uczen;
import sda.spring.core.service.UczenService;

@Slf4j
public class Main2 {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(Main2.class);

        // retrieve configured instance
        UczenService service = context.getBean("uczenService", UczenService.class);

        // use configured instance
        Uczen uczen = service.zapiszNowegoUczniaDoSzkoly("Adam", "Kowalski", "90120108729");
        log.debug(uczen.toString());
    }

    // zadania:
    // 1. skonfigurować kontekst jak w zadaniu 1, ale korzystając z Java Config
    // 2. zainicjalizować poprawnie JPA w beanie entityManagerProvider korzystając z interfejsów InitializingBean i DisposableBean
}
