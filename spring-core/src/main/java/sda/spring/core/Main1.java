package sda.spring.core;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import sda.spring.core.domain.Uczen;
import sda.spring.core.service.UczenService;

@Slf4j
public class Main1 {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext("appContext.xml");

        // retrieve configured instance
        UczenService service = context.getBean("uczenService", UczenService.class);

        // use configured instance
        Uczen uczen = service.zapiszNowegoUczniaDoSzkoly("Adam", "Kowalski", "90120108729");
        log.debug(uczen.toString());
    }

    // zadania:
    // 1. skonfigurować kontekst w pliku appContext.xml definiując beany: userService, userRepository i entityManagerProvider
    // 2. zainicjalizować poprawnie JPA w beanie entityManagerProvider
    // 3. zmienić metodę main tak, aby kontekst był poprawnie zamykany nawet w przypadku wyjątku
    // 4. utworzyć w bazie prostą strukturę, aby uczeń zapisywał się
}
