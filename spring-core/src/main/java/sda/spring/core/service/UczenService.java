package sda.spring.core.service;

import sda.spring.core.domain.Uczen;
import sda.spring.core.repository.UczenRepository;

public class UczenService {

    private UczenRepository uczenRepository;
    private EntityManagerProvider entityManagerProvider;

    public Uczen zapiszNowegoUczniaDoSzkoly(String imie, String nazwisko, String pesel) {
        entityManagerProvider.startNewEm();
        entityManagerProvider.startTransaction();

        Uczen uczen = uczenRepository.create(Uczen.builder()
                .imie(imie)
                .nazwisko(nazwisko)
                .pesel(pesel)
                .build());

        entityManagerProvider.commitTransaction();
        entityManagerProvider.endCurrentEm();

        return uczen;
    }
}
