package sda.spring.core.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;

import java.util.Locale;

@Slf4j
public class ServiceA {

    private MessageSource messageSource;

    public void init() {
        log.error(messageSource.getMessage("hello", null, Locale.getDefault()));
    }
}
