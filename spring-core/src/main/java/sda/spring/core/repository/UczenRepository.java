package sda.spring.core.repository;

import sda.spring.core.domain.Uczen;
import sda.spring.core.service.EntityManagerProvider;

public class UczenRepository {

    private EntityManagerProvider entityManagerProvider;

    public Uczen create(Uczen uczen) {
        entityManagerProvider.getCurrentEm().persist(uczen);
        return uczen;
    }

    public Uczen read(Long id) {
        return null;
    }

    public void update(Uczen uczen) {

    }

    public void delete(Uczen uczen) {

    }

    public void delete(long id) {

    }
}
