package sda.spring.core;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
@Slf4j
public class Main5 {

    public static void main(String[] args) {
        new SpringApplicationBuilder(Main5.class)
                .run(args);
    }

    // zadania:
    // 1. skonfigurowac serwis A, by wyswietlal wiadomosc spod klucza "hello" podczas startu
    // 2a. dodac drugi serwis, wyswietlajacy wiadomosc spod klucza "name"
    // 2b. dodac drugi serwis, wyswietlajacy wiadomosc spod klucza "name", z polskimi localami
    // 3. serwis A ma byc dodawany do kontekstu tylko w profilu "profileA"
    // 4. serwis B ma byc dodawany do kontekstu tylko w profilu "profileB"
    // 5. utworzyć serwis, który wyświetli parametry z pliku application.properties korzystając z Environment
    // 6a. zmapowac ustawienia z pliku application.properties na klasę SdaProperties
    // 6b. utworzyć serwis, który wyświetli parametry z pliku application.properties korzystając z klasy SdaProperties
    // 7. utworzyć plik z parametrami dla profilu "profileC" i nadpisać w nim wartość jednego parametru
    // 8. nadpisać wartośc parametru z użyciem parametru JavaVM
    // 9. nadpisać wartość parametru z użyciem zmiennej środowiskowej
}
