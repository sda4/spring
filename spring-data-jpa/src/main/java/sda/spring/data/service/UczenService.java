package sda.spring.data.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import sda.spring.data.domain.Uczen;
import sda.spring.data.repository.UczenRepository;
import sda.spring.data.repository.UczenRepositoryEm;
import sda.spring.data.repository.UczenRepositoryEmf;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class UczenService {

    @Autowired
    private UczenRepositoryEmf uczenRepositoryEmf;

    @Autowired
    private UczenRepositoryEm uczenRepositoryEm;

    @Autowired
    private UczenRepository uczenRepository;

    public Uczen zapiszNowegoUczniaDoSzkolyEmf(String imie, String nazwisko, String pesel) {
        Uczen uczen = uczenRepositoryEmf.create(Uczen.builder()
                .imie(imie)
                .nazwisko(nazwisko)
                .pesel(pesel)
                .build());

        return uczen;
    }

    public Uczen zapiszNowegoUczniaDoSzkolyEm(String imie, String nazwisko, String pesel) {
        Uczen uczen = uczenRepositoryEm.create(Uczen.builder()
                .imie(imie)
                .nazwisko(nazwisko)
                .pesel(pesel)
                .build());

        return uczen;
    }

    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void bezTransakcji() {

    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Uczen zapiszNowegoUczniaDoSzkoly(String imie, String nazwisko, String pesel, boolean wyjatek) {
        Uczen uczen = uczenRepository.save(Uczen.builder()
                .imie(imie)
                .nazwisko(nazwisko)
                .pesel(pesel)
                .build());

        if (wyjatek) {
            throw new RuntimeException();
        }

        return uczen;
    }

    @Transactional(propagation = Propagation.REQUIRED)
    public Uczen zapiszNowegoUczniaDoSzkoly(String imie, String nazwisko, String pesel) {
        Uczen uczen = uczenRepository.save(Uczen.builder()
                .imie(imie)
                .nazwisko(nazwisko)
                .pesel(pesel)
                .build());

        return uczen;
    }

    public void crud() {
        Uczen uczen = uczenRepository.save(Uczen.builder()
                .imie("Kamil")
                .nazwisko("Jasiński")
                .pesel("90040312345")
                .build());

        Uczen uczen2 = uczenRepository.findOne(uczen.getIdUcznia());
        uczen2.setImie("Patryk");
        uczenRepository.save(uczen2);
        uczenRepository.delete(uczen2.getIdUcznia());
    }

    public Uczen wczytajUczniaPoPeselu(String pesel) {
        return uczenRepository.findFirstByPesel(pesel).get(0);
    }

    public List<Uczen> pobierzUczniowUrodzonychMiedzy(Date dataOd, Date dataDo) {
        return uczenRepository.findByDataUrodzeniaBetween(
                dataOd, dataDo);
    }

    public void pobierzUczniowUrodzonychMiedzyZeStronicowaniem(Date dataOd, Date dataDo) {
        Page<Uczen> page = uczenRepository.findByDataUrodzeniaBetween(
                dataOd, dataDo, new PageRequest(0, 2));

        boolean isNextPage;

        do {
            log.info("Numer strony: " + page.getNumber());
            page.getContent().forEach(uczen -> log.info(uczen.toString()));
            isNextPage = page.hasNext();

            if (isNextPage) {
                page = uczenRepository.findByDataUrodzeniaBetween(dataOd, dataDo, page.nextPageable());
            }
        } while (isNextPage);
    }


}
