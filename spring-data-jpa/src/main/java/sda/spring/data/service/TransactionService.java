package sda.spring.data.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sda.spring.data.domain.Uczen;

@Service
@Slf4j
public class TransactionService {

    @Autowired
    private UczenService uczenService;

    @Transactional
    public void zTransakcja() {
        uczenService.bezTransakcji();
    }

    public void zapiszDwochUczniow() {
        try {
            uczenService.zapiszNowegoUczniaDoSzkoly("Adam", "Bednarz", "1234", true);
        } catch (RuntimeException e) {
            log.error("wyjatek w transakcji");
        }
        uczenService.zapiszNowegoUczniaDoSzkoly("Stefan", "Burczymucha", "5678", false);
    }

    @Transactional
    public Uczen zapiszNowegoUczniaDoSzkoly(String imie, String nazwisko, String pesel) {
        uczenService.zapiszNowegoUczniaDoSzkoly(imie, nazwisko, pesel);
        uczenService.zapiszNowegoUczniaDoSzkoly(imie, nazwisko, pesel);
        uczenService.zapiszNowegoUczniaDoSzkoly(imie, nazwisko, pesel);
        uczenService.zapiszNowegoUczniaDoSzkoly(imie, nazwisko, pesel);


        return uczenService.zapiszNowegoUczniaDoSzkoly(imie, nazwisko, pesel);
    }
}
