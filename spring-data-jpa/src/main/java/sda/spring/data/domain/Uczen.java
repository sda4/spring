package sda.spring.data.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Uczen {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idUcznia;

    private String imie;
    private String nazwisko;
    private String pesel;

    private Date dataUrodzenia;

    @ManyToOne
    private Klasa klasa;
}
