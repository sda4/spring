package sda.spring.data;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import sda.spring.data.domain.Uczen;
import sda.spring.data.service.UczenService;

import java.util.Calendar;
import java.util.Date;

@Slf4j
@SpringBootApplication
public class Main2 {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder()
                .sources(Main2.class)
                .run(args);

        // retrieve configured instance
        UczenService service = context.getBean("uczenService", UczenService.class);

        // use configured instance

        //service.crud();

        Uczen uczen = service.wczytajUczniaPoPeselu("90120108729");
        log.info(uczen.toString());

        log.info("Ilosc uczniow: {}", service.pobierzUczniowUrodzonychMiedzy(
                date(1990, 1, 1),
                date(1990, 12, 31))
                .size());

        service.pobierzUczniowUrodzonychMiedzyZeStronicowaniem(
                date(1990, 1, 1),
                date(1990, 12, 31));
    }

    private static Date date(int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month - 1);
        cal.set(Calendar.DAY_OF_MONTH, day);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);

        return cal.getTime();
    }

    // zadania:
    // 1. wywołać przynajmniej RAZ każdą z metod CRUD
    // 2. dodać do repozytorium UczenRepository metodę do szukania po pesel'u i wywołać ją w metodzie UczenService.wczytajUczniaPoPeselu
    // 3. dodać do klasy Uczen pole dataUrodzenia
    // 4. dodać do repozytorium UczenRepository metodę do szukania uczniów urodzonych w zakresie dat i wywołać ją w metodzie UczenService.pobierzUczniowUrodzonychMiedzy
    // 5. zmienić (lub lepiej - dodać nową) powyższą metodę repozytorium, aby umożliwiała stronicowanie, wczytać pierwszą stronę o rozmiarze 5
    // 6. zminenić metodę do szukania po pesel'u, aby korzystała z JPQL

}
