package sda.spring.data;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import sda.spring.data.domain.Uczen;
import sda.spring.data.service.UczenService;

@Slf4j
@SpringBootApplication
public class Main1 {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(Main1.class)
                .run(args);

        // retrieve configured instance
        UczenService service = context.getBean("uczenService", UczenService.class);

        // use configured instance
        Uczen uczen1 = service.zapiszNowegoUczniaDoSzkolyEmf("Adam", "Kowalski", "90120108729");
        log.info(uczen1.toString());

        Uczen uczen2 = service.zapiszNowegoUczniaDoSzkolyEm("Adam", "Kowalski", "90120108729");
        log.info(uczen2.toString());


        Uczen uczen3 = service.zapiszNowegoUczniaDoSzkoly("Adam", "Kowalski", "90120108729");
        log.info(uczen3.toString());
    }

    // zadania:
    // 1. dokończyć konfigurację bean'ów i ich zależności
    // 2a. wstrzyknąć do repozytorium UczenRepository obiekt EntityManagerFactory korzystając z adnotacji @PersistenceUnit
    // 2b. dokończyć implementację metody UczenRepository.create korzystając z wstrzykniętego EMF
    // 3a. zamiast EMF, wstrzyknąć do repozytorium UczenRepository obiekt EntityManager, korzystając z adnotacji @PersistenceContext
    // 3b. odpowiednio zmodyfikować metodę UczenRepository.create aby korzystała z wstrzykniętego EM
    // 4. przerobić UczenRepository aby było repozytorium Spring Data
}
