package sda.spring.data.repository;

import org.springframework.stereotype.Component;
import sda.spring.data.domain.Uczen;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;

@Component
public class UczenRepositoryEmf {

    @PersistenceUnit
    private EntityManagerFactory emf;

    public Uczen create(Uczen uczen) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(uczen);
        em.getTransaction().commit();
        em.close();

        return uczen;
    }
}
