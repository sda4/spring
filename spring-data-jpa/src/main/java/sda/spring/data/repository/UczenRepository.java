package sda.spring.data.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import sda.spring.data.domain.Uczen;

import java.util.Date;
import java.util.List;

@Service
public interface UczenRepository extends PagingAndSortingRepository<Uczen, Long> {
    @Query("select u from Uczen u where u.pesel=:pesel")
    Uczen findFirstByPeselUsingQuery(@Param("pesel") String pesel);

    List<Uczen> findFirstByPesel(String pesel);

    List<Uczen> findByDataUrodzeniaBetween(Date from, Date to);
    Page<Uczen> findByDataUrodzeniaBetween(Date from, Date to, Pageable pageable);

    List<Uczen> findByKlasaIdKlasyOrderByDataUrodzeniaDesc(Long idKlasy);
    List<Uczen> findByKlasaRokRozpoczeciaNaukiAndKlasaSymbol(Integer rok, String symbol);
}
