package sda.spring.data.repository;

import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import sda.spring.data.domain.Uczen;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Component
public class UczenRepositoryEm {

    @PersistenceContext
    private EntityManager em;

    @Transactional
    public Uczen create(Uczen uczen) {
        em.persist(uczen);

        return uczen;
    }
}
