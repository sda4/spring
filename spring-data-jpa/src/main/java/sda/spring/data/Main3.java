package sda.spring.data;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import sda.spring.data.repository.UczenRepository;

@Slf4j
@SpringBootApplication
public class Main3 {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder()
                .sources(Main3.class)
                .run(args);

        UczenRepository uczenRepository = context.getBean("uczenRepository", UczenRepository.class);
        //log.info("ilosc uczniow w klasie A: " + uczenRepository.findByKlasaIdKlasy(1L).size());
        log.info("ilosc uczniow w klasie B z 2011: " + uczenRepository.findByKlasaRokRozpoczeciaNaukiAndKlasaSymbol(2011, "B").size());
    }

    // zadania:
    // 1. dodać encje Klasa grupującą uczniów, z atrybutami "rokRozpoczeciaNauki" oraz "symbol"
    // 2. uzupełnić bazę danymi
    // 3. dodać metodę szukającą uczniów z określonej klasy (po ID), sortując po dacie urodzenia
    // 4. dodać metodę szukającą uczniów z określonej klasy (po roku rozpoczęcia i symbolu)

}
