package sda.spring.data;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import sda.spring.data.service.TransactionService;

@Slf4j
@SpringBootApplication
public class Main4 {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder()
                .sources(Main4.class)
                .run(args);

        // retrieve configured instance
        TransactionService service = context.getBean("transactionService", TransactionService.class);

        // use configured instance
        //Uczen uczen = service.zapiszNowegoUczniaDoSzkoly("Adam", "Kowalski", "90120108729");
        //log.info(uczen.toString());

        service.zapiszDwochUczniow();
    }

    // zadania:
    // 1. pozbyć się błędu "No existing transaction found for transaction marked with propagation 'mandatory'"
    // 2. zademonstrować działanie propagation=NEVER na nowej metodzie serwisu UczenService
    // 3. dodać dwóch uczniów w dwóch niezależnych transakcjach
    // 4. dodać dwóch uczniów w dwóch niezależnych transakcjach, z których pierwsza kończy się rollback'iem
}
