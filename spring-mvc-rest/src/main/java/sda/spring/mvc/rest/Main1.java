package sda.spring.mvc.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@Slf4j
@SpringBootApplication
public class Main1 {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder()
                .sources(Main1.class)
                .run(args);
    }

    // zadania:
    // korzystając z automatycznej implementacji data-rest:
    // 1. dodać klasę
    // 2. dodać ucznia
    // 3. przypisać ucznia do klasy
    // 4. wyświetlić wszystkich uczniów używając stronicowania
    // 5. wyświetlić uczniów z klasy o okreslonym symbolu i roku rozpoczęcia
}
