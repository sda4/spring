package sda.spring.mvc.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;
import sda.spring.mvc.rest.domain.Uczen;
import sda.spring.mvc.rest.repository.KlasaRepository;
import sda.spring.mvc.rest.repository.UczenRepository;

import java.util.Optional;

@RestController
@RequestMapping("/uczniowie")
public class UczenController {

    @Autowired
    private UczenRepository uczenRepository;

    @Autowired
    private KlasaRepository klasaRepository;

    @GetMapping("/{idUcznia}")
    public ResponseEntity pobierzUcznia(@PathVariable("idUcznia") Long xxx) {
        Uczen uczen = uczenRepository.findOne(xxx);
        if (uczen != null) {
            return ResponseEntity.ok(uczen);
        } else {
            return new ResponseEntity<>("Nie znaleziono ucznia", HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public ResponseEntity zapiszUcznia(@RequestBody Uczen uczen) {
        if (!StringUtils.isEmpty(uczen.getImie())) {
            uczen = uczenRepository.save(uczen);

            return ResponseEntity.created(MvcUriComponentsBuilder.fromMethodName(UczenController.class,
                    "pobierzUcznia", uczen.getIdUcznia()).build().toUri())
                    .build();
        } else {
            return new ResponseEntity("Nie podałeś imienia !!!", HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @ExceptionHandler(NieZnalezionoUczniaException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String obsluzWyjatekNieZnalezionoUcznia(NieZnalezionoUczniaException ex) {
        return "Nie znaleziono tego ucznia: " + ex.getId();
    }

    @ExceptionHandler(NieZnalezionoKlasyException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public String obsluzWyjatekNieZnalezionoKlasy(NieZnalezionoKlasyException ex) {
        return "Nie znaleziono tej klasy: " + ex.getId();
    }

    @DeleteMapping("/{idUcznia}")
    public void usunUcznia(@PathVariable("idUcznia") Long xxx) {
        uczenRepository.delete(Optional.ofNullable(uczenRepository.findOne(xxx))
                .orElseThrow(() -> new NieZnalezionoUczniaException(xxx)));
    }

    @GetMapping
    public Iterable<Uczen> listaUczniow(Pageable pageable) {
        return uczenRepository.findAll(pageable);
    }

    @PutMapping("/{idUcznia}/doKlasy")
    public void dodajDoKlasy(@PathVariable("idUcznia") Long idUcznia, @RequestParam("klasa") Long idKlasy) {
        Optional.ofNullable(uczenRepository.findOne(idUcznia))
                .orElseThrow(() -> new NieZnalezionoUczniaException(idUcznia))
            .setKlasa(Optional.ofNullable(klasaRepository.findOne(idKlasy))
                .orElseThrow(() -> new NieZnalezionoKlasyException(idKlasy)));

//        Uczen uczen = uczenRepository.findOne(idUcznia);
//        if (uczen!=null) {
//            Klasa klasa = klasaRepository.findOne(idKlasy);
//            if (klasa != null) {
//                uczen.setKlasa(klasa);
//            } else {
//                throw new NieZnalezionoKlasyException(idKlasy)
//            }
//        } else {
//            throw new NieZnalezionoUczniaException(idUcznia);
//        }
    }

    @GetMapping("/szukaj")
    public Iterable<Uczen> listaUcznowSzukaj(
            @RequestParam(name = "symbol", required = false) String symbol,
            @RequestParam(name = "rok", required = false) Integer rok) {

        if (rok != null && symbol != null) {
            return uczenRepository.findByKlasaRokRozpoczeciaNaukiAndKlasaSymbol(rok, symbol);
        } else if (rok != null) {
            return uczenRepository.findByKlasaRokRozpoczeciaNauki(rok);
        } else if (symbol != null) {
            return uczenRepository.findByKlasaSymbol(symbol);
        } else {
            return uczenRepository.findAll();
        }
    }
}
