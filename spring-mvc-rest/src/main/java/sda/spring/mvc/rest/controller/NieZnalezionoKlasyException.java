package sda.spring.mvc.rest.controller;

public class NieZnalezionoKlasyException extends RuntimeException {
    private Long id;

    public NieZnalezionoKlasyException(Long idKlasy) {
        super();
        this.id=idKlasy;
    }

    public Long getId() {
        return id;
    }
}
