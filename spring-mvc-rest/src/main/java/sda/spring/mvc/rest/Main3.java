package sda.spring.mvc.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@Slf4j
@SpringBootApplication
public class Main3 {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder()
                .sources(Main3.class)
                .run(args);
    }

    // zadania:
    // 1. dodać obsługę błędu do kontrolera z punktu 2 poprzedniego ćwiczenia (gdy nie znaleziono ucznia do usunięcia) - z wykorzystaniem ErrorHandler'a
    // 2. dodać obsługę błędu do kontrolera z punktu 6 poprzedniego ćwiczenia (z wykorzystaniem ResponseEntity)
}
