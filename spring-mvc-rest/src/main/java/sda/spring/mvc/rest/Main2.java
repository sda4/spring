package sda.spring.mvc.rest;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;

@Slf4j
@SpringBootApplication
public class Main2 {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder()
                .sources(Main2.class)
                .run(args);
    }

    // zadania:
    // 1. utworzyć kontroler do wyświetlania pojedynczego ucznia (ID ucznia jako parametr)
    // 2. utworzyć kontroler do zapisu ucznia, który zwróci zapisany rekord
    // 3. utworzyć kontroler do kasowania ucznia (ID ucznia jako element ścieżki)
    // 4. utworzyć kontroler do wyświetlania wszystkich uczniów, ze stronicowaniem
    // 5. utworzyć kontroler do wyświetlania uczniów z klasy o okreslonym symbolu i roku rozpoczęcia
    // 6. utworzyć kontroler do przypisania ucznia do klasy o określonym id
    // 7. zmienić kontroler z punktu 2, aby zwracał URL do utworzonego zasobu (użyć MvcUriComponentsBuilder)
}
