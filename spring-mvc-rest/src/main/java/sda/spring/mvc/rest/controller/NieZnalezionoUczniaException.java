package sda.spring.mvc.rest.controller;

public class NieZnalezionoUczniaException extends RuntimeException {
    private Long id;

    public NieZnalezionoUczniaException(Long idUcznia) {
        super();
        this.id=idUcznia;
    }

    public Long getId() {
        return id;
    }
}
