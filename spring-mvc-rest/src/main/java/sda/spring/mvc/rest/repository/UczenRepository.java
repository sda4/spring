package sda.spring.mvc.rest.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sda.spring.mvc.rest.domain.Uczen;

import java.util.List;

@Repository
public interface UczenRepository extends PagingAndSortingRepository<Uczen, Long> {
    Page<Uczen> findByKlasaRokRozpoczeciaNaukiAndKlasaSymbol(
            @Param("rokRozpoczeciaNauki") Integer rokRozpoczeciaNauki,
            @Param("symbol") String symbol,
            Pageable pageable);

    List<Uczen> findByKlasaRokRozpoczeciaNaukiAndKlasaSymbol(
            @Param("rokRozpoczeciaNauki") Integer rokRozpoczeciaNauki,
            @Param("symbol") String symbol);

    List<Uczen> findByKlasaRokRozpoczeciaNauki(
            @Param("rokRozpoczeciaNauki") Integer rokRozpoczeciaNauki);


    List<Uczen> findByKlasaSymbol(
            @Param("symbol") String symbol);

}
