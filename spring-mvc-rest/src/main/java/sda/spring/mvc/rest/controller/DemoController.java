package sda.spring.mvc.rest.controller;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

@RestController
@RequestMapping("/demo")
public class DemoController {

    @RequestMapping("/{param}")
    public String demo(@PathVariable("param") String param) {
        return MvcUriComponentsBuilder.fromMethodName(DemoController.class,
                "demo", "123").toUriString();
    }
}
