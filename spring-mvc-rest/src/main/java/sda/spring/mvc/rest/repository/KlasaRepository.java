package sda.spring.mvc.rest.repository;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import sda.spring.mvc.rest.domain.Klasa;

@Repository
public interface KlasaRepository extends PagingAndSortingRepository<Klasa, Long> {

}
